#include "RubixWindow.hpp"
#include "odgl_Cube.hpp"
#include "odgl_View.hpp"
#include "odgl_Light.hpp"
#include "odgl_Group.hpp"
#include "odgl_Vector.hpp"

#include <iostream>

using namespace OpenDoorGL;

int main(void)
{
    WindowInterface *mainWindow = new RubixCubeWindow();
    mainWindow->InitWindow(
        "Stacked Cube Test",
        true,
        1024,
        768,
        800,
        200);

    View *testView = new View();
    testView->view = glm::lookAt(
        glm::vec3(4, 3, 3), // Camera is at (4,3,3), in World Space
        glm::vec3(0, 0, 0), // and looks at the origin
        glm::vec3(0, 1, 0)  // Head is up (set to 0,-1,0 to look upside-down)
    );

    testView->proj = glm::perspective(glm::radians(45.0f), 4.0f / 3.0f, 0.1f, 100.0f);
    testView->position = glm::vec3(4, 3, 3);
    testView->verticalAngle = -0.6f;
    testView->horizontalAngle = 4.0f;

    Cube *cube1 = new Cube(true, true);
    cube1->setColor(1.0f, 0.0f, 0.0f);
    cube1->setSize(1.0);
    Cube *cube2 = new Cube(true, true);
    cube2->setColor(0.0f, 1.0f, 0.0f);
    cube2->setSize(1.0);
    cube2->Translate(-1.0f, 0.0f, 0.0f);
    Cube *cube3 = new Cube(true, true);
    cube3->setColor(0.0f, 0.0f, 1.0f);
    cube3->setSize(2.0);
    cube3->Translate(1.5f, 0.5f, -0.5f);

    Cube *cube4 = new Cube(true, false);
    cube4->setColor(0.0f, 1.0f, 1.0f);
    cube4->setSize(0.2);
    cube4->Translate(5.0f, 0.5f, -0.5f);

    Light *light = new Light();
    cube1->lights.push_back(light);
    cube2->lights.push_back(light);
    cube3->lights.push_back(light);

    Group *group = new Group();

    mainWindow->SetView(testView);
    group->InsertObject(cube1);
    group->InsertObject(cube2);
    group->InsertObject(cube3);
    mainWindow->InsertObject(group);
    mainWindow->InsertObject(cube4);

    while (mainWindow->AppRunning())
    {
        group->Rotate(0.5, 0, 0, 1);
        cube2->Rotate(-.2, 1, 0, 0);
        cube4->Rotate(.7, 0, 1, 0, -5.0, 0, 0);
        light->position = glm::vec3(cube4->getCenterPoint().getX(), cube4->getCenterPoint().getY(), cube4->getCenterPoint().getZ());
        mainWindow->UpdateFrame();
        mainWindow->RenderFrame();
    }

    mainWindow->CleanUp();
}