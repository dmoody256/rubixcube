#include "RubixWindow.hpp"

double RubixCubeWindow::RenderFrame()
{
    ComputeMatricesFromInputs();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    topGroup->draw(_currentView);
    // Swap buffers
    glfwSwapBuffers(window);
    glfwPollEvents();

    double currentTime = glfwGetTime();
    double deltaTime = glfwGetTime() - _renderTime;
    _renderTime = currentTime;

    if (_frameRateEnabled)
    {
        _numFrames++;
        if (currentTime - _framePrintTime >= 1.0)
        { // If last prinf() was more than 1 sec ago
            // printf and reset timer
            printf("%f ms/frame\n", 1000.0 / double(_numFrames));
            _numFrames = 0;
            _framePrintTime += 1.0;
        }
    }

    return deltaTime;
}
