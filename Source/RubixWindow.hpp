#include "odgl_GLFW3.hpp"

class RubixCubeWindow : public OpenDoorGL::GLFW3Window
{
  public:
    double RenderFrame() override;
};